package com.Diplomski.WrapperService.Handlers;

import com.Diplomski.WrapperService.Handlers.Contracts.IHandler;
import com.Diplomski.WrapperService.Handlers.Exceptions.*;

import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.RequiredArgsConstructor;
import net.openhft.compiler.CompilerUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.util.ResourceUtils;

@RequiredArgsConstructor
public class JavaHandler implements IHandler {

    private final String userCodePath;

    private static final String CLASS_NAME_REGEX = "public[\\t\\s\\r\\n]+(static[\\t\\s\\r\\n]+)?class[\\t\\s\\r\\n]+([A-Za-z]+[A-Za-z0-9]*)[\\t\\s\\r\\n]+\\{";
    private static final Pattern CLASS_PATTERN = Pattern.compile(CLASS_NAME_REGEX);

    @Override
    public Object execute(List<Object> parameters) {
        try {
            String userCodeString = FileUtils.readFileToString(ResourceUtils.getFile(userCodePath), Charset.defaultCharset());
            final String mainClassName = getClassName(userCodeString);
            Class userClass = CompilerUtils.CACHED_COMPILER.loadFromJava(mainClassName, userCodeString);

            Method mainMethod = Arrays.stream(userClass.getMethods())
                    .filter(method -> method.getName().equals("main"))
                    .findFirst()
                    .orElseThrow(NoMainMethodFoundException::new);

            return mainMethod.invoke(userClass.getDeclaredConstructor().newInstance(), parameters.toArray());
        } catch (IllegalArgumentException exception) {
            throw new BadRequestException(exception.getMessage());
        }
        catch (Exception exception) {
            throw new ExecutionFailedException(exception.getMessage());
        }
    }

    @Override
    public String getUserCode() {
        try {
            return FileUtils.readFileToString(ResourceUtils.getFile(userCodePath), Charset.defaultCharset());
        } catch (IOException exception) {
            throw new UserCodeNotFoundException(exception);
        }
    }

    private String getClassName(String userCodeString) {
        Matcher matcher = CLASS_PATTERN.matcher(userCodeString);

        if (matcher.find()) {
            return matcher.group(2);
        }

        throw new NoMainClassFoundException();
    }
}
