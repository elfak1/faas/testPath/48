package com.Diplomski.WrapperService.Configurations;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

import static com.Diplomski.WrapperService.Configurations.InjectedConstants.PROJECT_ID;

@Slf4j
@Component
public class RegisterProjectService {

    private final RestTemplate restTemplate;

    public static final String AUTH_HEADER_NAME = "x-secret-api-key";
    public static final String SECRET_API_KEY = "GX42bVUgHpYQ8RFANdGGNh6W";

    public RegisterProjectService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @EventListener(ApplicationReadyEvent.class)
    public void loadData()
    {
        String url = String.format("http://elfakfaas.xyz/api/project/private/%s/running", PROJECT_ID);

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set(AUTH_HEADER_NAME, SECRET_API_KEY);
            HttpEntity<String> httpEntity = new HttpEntity<>("", headers);
            restTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
            log.info("Registered to project service successfully");
        } catch (Exception exception) {
            log.error("Failed to contact to project service on startup");
        }

    }
}
