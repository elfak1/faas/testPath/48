FROM adoptopenjdk:8-jdk as builder
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} application.jar
RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../../application.jar)

FROM adoptopenjdk:8-jdk
ARG DEPENDENCY=target/dependency
COPY --from=builder ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=builder ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=builder ${DEPENDENCY}/BOOT-INF/classes /app
ENTRYPOINT ["java","-cp","app:app/lib/*","com.Diplomski.WrapperService.WrapperServiceApplication"]
